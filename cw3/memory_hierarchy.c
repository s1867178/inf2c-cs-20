/*************************************************************************************|
|   1. YOU ARE NOT ALLOWED TO SHARE/PUBLISH YOUR CODE (e.g., post on piazza or online)|
|   2. Fill memory_hierarchy.c                                                        |
|   3. Do not use any other .c files neither alter mipssim.h or parser.h              |
|   4. Do not include any other library files                                         |
|*************************************************************************************/

#include "mipssim.h"

uint32_t cache_type = 0;

int OFFSET_BITS = 4;
int INDEX_BITS;
int TAG_BITS;

int* lru_list;
int lru_length = 0;

struct direct_mapped_cache {
    int valid;
    int tag;
    int data[16/4]; //containts array of 4 ints to represent 4 words
};

struct fully_associative_cache {
    int valid;
    int tag;
    int data[4];
};

struct direct_mapped_cache* dmc;
struct fully_associative_cache* fac;


void memory_state_init(struct architectural_state *arch_state_ptr)
{
    arch_state_ptr->memory = (uint32_t *) malloc(sizeof(uint32_t) * MEMORY_WORD_NUM);
    memset(arch_state_ptr->memory, 0, sizeof(uint32_t) * MEMORY_WORD_NUM);

    if (cache_size == 0) {
        // CACHE DISABLED
        memory_stats_init(arch_state_ptr, 0); // WARNING: we initialize for no cache 0
    } else {
        // CACHE ENABLED
        //assert(0); /// @students: remove assert and initialize cache
        //TODO: Init cache

        /// @students: memory_stats_init(arch_state_ptr, X); <-- fill # of tag bits for cache 'X' correctly

        int block_count;

        int list_size = cache_size / 16;
        lru_list = (int*)calloc(list_size, sizeof(int));
        memset(lru_list, -1, list_size);

        switch(cache_type) {
            case CACHE_TYPE_DIRECT: // direct mapped
                INDEX_BITS = (int) ceil(log2(cache_size)) - OFFSET_BITS;
                TAG_BITS = 32 - INDEX_BITS - OFFSET_BITS ;
                block_count = (int) cache_size / 16;

                dmc = calloc(block_count, sizeof(struct direct_mapped_cache));
                break;
            case CACHE_TYPE_FULLY_ASSOC: // fully associative
                TAG_BITS = 32 - OFFSET_BITS;
                block_count = (int) cache_size / 16;

                fac = calloc(block_count, sizeof(struct fully_associative_cache));
                break;
            case CACHE_TYPE_2_WAY: // 2-way associative
                break;
            default:
                assert(0);
        }

        memory_stats_init(arch_state_ptr, TAG_BITS);
    }
}

// returns data on memory[address / 4]
int memory_read(int address){
    arch_state.mem_stats.lw_total++;
    check_address_is_word_aligned(address);

    if (cache_size == 0) {
        // CACHE DISABLED
        return (int) arch_state.memory[address / 4];
    } else {
        // CACHE ENABLED

        /// @students: your implementation must properly increment: arch_state_ptr->mem_stats.lw_cache_hits
        int offset;
        int index;
        int tag;
        int data_address;

        int is_flagged = 0;
        int flagged_index;

        int blocks;
        int oldest_index;

        switch(cache_type) {
            case CACHE_TYPE_DIRECT: // direct mapped
                offset = get_piece_of_a_word(address, 0, OFFSET_BITS) / 4;
                index = get_piece_of_a_word(address, OFFSET_BITS, INDEX_BITS);
                tag = get_piece_of_a_word(address, OFFSET_BITS + INDEX_BITS, TAG_BITS);
                data_address = 0xFFFFFFF0 & address;

                if (dmc[index].valid == 1 && dmc[index].tag == tag) {
                    arch_state.mem_stats.lw_cache_hits += 1;
                } else {
                    dmc[index].valid = 1;
                    dmc[index].tag = tag;

                    for (int i = 0; i < 4; i++) {
                        dmc[index].data[i] = (int) arch_state.memory[(data_address / 4) + i];
                    }
                }
                return dmc[index].data[offset];
                break;
            case CACHE_TYPE_FULLY_ASSOC: // fully associative
                offset = get_piece_of_a_word(address, 0, OFFSET_BITS) / 4;
                tag = get_piece_of_a_word(address, OFFSET_BITS + INDEX_BITS, TAG_BITS);
                blocks = cache_size / 16;

                for (int i = 0; i < blocks; i++) {
                    struct fully_associative_cache block = fac[i];

                    if (block.valid == 1 && block.tag == tag) {

                        //printf("index %d \n", i); //the index is always 0

                        is_flagged = 0;

                        for (int j = 0; j < lru_length; j++) {
                            if (lru_list[j] == i) {
                                is_flagged = 1;
                                flagged_index = j;
                            }
                        }

                        if (is_flagged == 1) {
                            for (int k = flagged_index; k < lru_length - 1; k++) {
                                lru_list[k] = lru_list[k+1];
                            }
                            lru_list[lru_length] = i;
                        } else {
                             //printf("not entering here\n\n\n");
                            lru_list[lru_length] = i;
                            lru_length += 1;
                        }

                        arch_state.mem_stats.lw_cache_hits += 1;

                        return block.data[offset];
                    }
                }
                oldest_index = lru_list[0];

                if (lru_list[0] == -1) {
                    oldest_index = 0;
                    lru_list[0] = 0;
                } else {
                    oldest_index = lru_list[0];
                }

                fac[oldest_index].valid = 1;
                fac[oldest_index].tag = tag;


                for (int i = 0; i < 4; i++) {
                    fac[oldest_index].data[i] = (int) arch_state.memory[((address / 4) - offset) + i];
                }

                //all the elements of the array shifts by 1, deleting the first element (that will be put at the end of the array)
                for (int i = 0; i < lru_length - 1; i++) {
                    lru_list[i] = lru_list[i+1];
                }

                //puts it at the end as it is now the most recently used
                lru_list[lru_length - 1] = oldest_index;

                //printf("least index %d", oldest_index);

                return fac[oldest_index].data[offset];
                break;
            case CACHE_TYPE_2_WAY: // 2-way associative
                break;
        }
    }
    return 0;
}

// writes data on memory[address / 4]
void memory_write(int address, int write_data) {
    arch_state.mem_stats.sw_total++;
    check_address_is_word_aligned(address);

    if (cache_size == 0) {
        // CACHE DISABLED
        printf("Index: %d \n", address / 4);
        arch_state.memory[address / 4] = (uint32_t) write_data;
    } else {
        // CACHE ENABLED
        //assert(0); /// @students: Remove assert(0); and implement Memory hierarchy w/ cache

        /// @students: your implementation must properly increment: arch_state_ptr->mem_stats.sw_cache_hits
        int index;
        int tag;
        int offset;

        switch(cache_type) {
        case CACHE_TYPE_DIRECT: // direct mapped
            offset = get_piece_of_a_word(address, 0, OFFSET_BITS) / 4;
            index = get_piece_of_a_word(address, OFFSET_BITS, INDEX_BITS);
            tag = get_piece_of_a_word(address, OFFSET_BITS + INDEX_BITS, TAG_BITS);

            if (dmc[index].valid == 1 && dmc[index].tag == tag) {
                arch_state.mem_stats.sw_cache_hits += 1;

                dmc[index].data[offset] = write_data;

            }
            arch_state.memory[address / 4] = (int) write_data;

            break;
        case CACHE_TYPE_FULLY_ASSOC: // fully associative
            offset = get_piece_of_a_word(address, 0, OFFSET_BITS) / 4;
            tag = get_piece_of_a_word(address, OFFSET_BITS + INDEX_BITS, TAG_BITS);

            int blocks = cache_size / 16;

            for (int i = 0; i < blocks; i++) {
                struct fully_associative_cache block = fac[i];

                // hit scenario
                if (block.valid == 1 && block.tag == tag) {
                    arch_state.mem_stats.sw_cache_hits += 1;

                    fac[i].data[offset] = write_data;
                    return;
                }
            }
            // miss scenario
           // write to memory
            arch_state.memory[address / 4] = (int) write_data;

            break;
        case CACHE_TYPE_2_WAY: // 2-way associative
            break;
        }
    }
}
